import Exceptions.CuaBuida;
import Exceptions.CuaPlena;

/**
 * Classe que implementa la cua circular amb un vector i dos punters
 * @author Sergi Quevedo i Javier Ortega
 * @param <E> per tal de poder definir la cua circular generica
 */
public class CuaCircular<E> implements TADCuaGenerica<E> {
	
	private int numElem;
	private E[] vector;
	private int primer;
	private int ultim;
	
	/** Constructor de la classe cua circular, rep per parametre la mida maxima del
	 * vector, inicialitza el vector generic  amb la mida maxima i estableix 
	 * els dos punters incialment a la posicio 0 i el numero d'elements a 0.
	 * @param max mida maxima del vector. **/
	@SuppressWarnings("unchecked")
	public CuaCircular(int dim){
		vector=(E[]) new Object[dim];
		numElem=0;
		primer=0;
		ultim=0;
	}
	
	/** Afegeix un element al final de la cua. 
	 * @param e element a encuar.
	 * @throws CuaPlena en cas que no es pugui afegir perque el vector esta ple.**/
	public void Encuar(E e) throws CuaPlena{
		if(numElem==vector.length) throw new CuaPlena();
		else if(numElem==0){
			vector[ultim]=e;
			ultim=(ultim+1)%vector.length;
			numElem++;
		}else{
			if(numElem<vector.length){
				vector[ultim]=e;
				ultim=(ultim+1)%vector.length;
				numElem++;
			}
		}
	}
	
	/** Retorna el primer element de la cua i l'elimina.
	 * @return primer element de la cua.
	 * @throws CuaBuida en cas que no hi hagi cap element a desencuar. **/
	public E Desencuar() throws CuaBuida{
		E aux=null;
		if(esBuida()) throw new CuaBuida();
		else if(numElem>0){
			aux=vector[primer];
			numElem--;
			primer=(primer+1)%vector.length;
		}
		return(aux);
	}
	
	/** Retorna l'element mes antic de la cua.
	 * @return primer element a la cua.
	 * @throws CuaBuida en cas que no hi hagi cap element a la cua. **/
	public E primer() throws CuaBuida{
		if(esBuida()) throw new CuaBuida();
		else return(vector[primer]);
	}
	
	/** Retorna l'estat de la cua, si es buida o no.
	 * @return true en cas que cua sigui buida i false en cas que la cua
	 * no sigui buida. **/
	public boolean esBuida(){
		return(numElem==0);
	}
	
	/**
	 * Retorna el numero d'elements que te la cua.
	 */
	public int getNumElem(){
		return(numElem);
	}
}
