
/** 
 * Classe per tal d'implementar la cua dinamica.
 * @author Sergi Quevedo i Javier Ortega
 * @param <E> es per tal de fer la cua dinamica generica
 */
public class CuaDinamica<E> implements TADCuaGenerica<E> {
	
	private Node<E> primer;
	private Node<E> ultim;
	private int numElem;
	
	/** Constructor de la classe cua dinamica. Inicialitza el primer i l'ultim node
	 *  a null i el numero d'elements a 0.*/
	public CuaDinamica(){
		this.primer=null;
		this.ultim=null;
		numElem=0;
	}
	
	/** Afegeix un element al final de la cua. 
	 * @param e element a encuar.*/
	public void Encuar(E e){
		Node<E> aux=new Node<E>(e, null);
		aux.setValor(e);
		aux.setSeg(null);
		if(primer == null){
			primer=aux;
			ultim=aux;
		}
		else{
			ultim.setSeg(aux);
			ultim=aux;
		}
		
	}
	
	/** Retorna el primer element de la cua i l'elimina.
	 * @return primer element de la cua.*/
	public E Desencuar(){
		E aux;
		aux=primer.getValor();
		primer=primer.getSeg();
		return(aux);
	}
	
	/** Retorna l'estat de la cua, si es buida o no.
	 * @return true en cas que cua sigui buida i false en cas que la cua
	 * no sigui buida. **/
	public boolean esBuida(){
		return(primer==null);
	}
	
	/** Retorna l'element mes antic de la cua.
	 * @return primer element a la cua.*/
	public E primer(){
		return(primer.getValor());
	}
	
	/**
	 * Retorna el numero d'elements que te la cua.
	 */
	public int getNumElem(){
		return(numElem);
	}
}
