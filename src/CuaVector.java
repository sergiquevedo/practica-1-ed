import Exceptions.CuaBuida;
import Exceptions.CuaPlena;

public class CuaVector<E> implements TADCuaGenerica<E> {
	
	private E[] vector;
	private int i;
	
	/** Constructor de la classe cua vector, rep per parametre la mida maxima del
	 * vector, inicialitza el vector generic  amb la mida maxima i estableix 
	 * el index d'elements a 0 i el numero d'elements a 0.
	 * @param max mida maxima del vector. **/
	@SuppressWarnings("unchecked")
	public CuaVector(int max) {
		vector = (E[]) new Object[max];
		i = 0;
	}
	
	/** Afegeix un element al final de la cua e incrementa l'index d'elements. 
	 * @param e element a encuar.
	 * @throws CuaPlena en cas que no es pugui afegir perque el vector esta ple.**/
	public void Encuar(E e) throws CuaPlena {
		if (i == vector.length) throw new CuaPlena();
		else{
			vector[i]=e;
			i++;
		}
	}

	/** Retorna el primer element de la cua, l'elimina i decrementa l'index
	 *  d'elements.
	 * @return primer element de la cua.
	 * @throws CuaBuida en cas que no hi hagi cap element a desencuar. **/
	public E Desencuar() throws CuaBuida {
		if (esBuida()) throw new CuaBuida();
		else{
			E primer = vector[0];
			if (i>1){
				for (int j=0; j>i;j++){
					vector[j]=vector[j+1];
				}
			}
			i--;
			return primer;
		}
	
	}

	/** Retorna l'element mes antic de la cua.
	 * @return primer element a la cua.
	 * @throws CuaBuida en cas que no hi hagi cap element a la cua. **/
	public E primer() throws CuaBuida{
		if (esBuida()) throw new CuaBuida();
		else return (vector[0]);
	}

	/** Retorna l'estat de la cua, si es buida o no.
	 * @return true en cas que cua sigui buida i false en cas que la cua
	 * no sigui buida. **/
	public boolean esBuida() {
		return (i==0);
	}
	
	/**
	 * Retorna el numero d'elements que te la cua
	 */
	public int getNumElem() {
		return(i);
	}

}
