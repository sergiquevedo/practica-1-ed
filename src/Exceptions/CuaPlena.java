package Exceptions;

/**
 * Classe creada per poder llan�ar l'excepcio per tenir la cua plena
 * @author Sergi Quevedo i Javier Ortega
 */
public class CuaPlena extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CuaPlena(){
		super("ERROR! S'ha assolit el nombre m�xim d'elements a la cua; est� plena.");
	}
}
