package Exceptions;

/**
 * Classe creada per a poder llan�ar l'excepcio per tenir la cua buida
 * @author Sergi Quevedo i Javier Ortega
 */
public class CuaBuida extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CuaBuida(){
		super("ERROR! No hi ha elements, la cua est� buida.");
	}
}
