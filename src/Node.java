/**
 * Classe node que s'utilitza a la cua dinamica.
 * @author Sergi Quevedo i Javier Ortega.
 * @param <E> �s mper fer el node generic.
 */
public class Node<E> {
	
	private E valor;
	private Node<E> seg;
	
	/**
	 * Constructor de la classe node on s'inicialitzen els atributs.
	 * @param valor
	 * @param seg
	 */
	public Node(E valor, Node<E> seg){
		this.valor=valor;
		seg=null;
	}
	
	//**********************************GETTER'S I SETTER'S DE LA CLASSE NODE***************************************//
	public E getValor() {
		return valor;
	}

	public void setValor(E valor) {
		this.valor = valor;
	}

	public Node<E> getSeg() {
		return seg;
	}

	public void setSeg(Node<E> seg) {
		this.seg = seg;
	}
	
	
}
