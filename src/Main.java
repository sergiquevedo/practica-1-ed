import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import Exceptions.CuaBuida;
import Exceptions.CuaPlena;

/**
 * Classe main on hi ha el programa principal i els metodes principals per tal d'executar correctament el programa
 * @author Sergi Quevedo i Javier Ortega
 */
public class Main{
	
	static InputStreamReader isr = new InputStreamReader(System.in);
	static BufferedReader teclat = new BufferedReader(isr);
	private static String clau;
	private static String textFitxer;
	private static String textFinal;
	private static String nomFitxer;
	private static int opcio=1;
	private static int mode;
	private static long tempsI;
	private static long tempsF;
	private static TADCuaGenerica<Integer> cua;
	
	/**
	 * Programa principal
	 * @param args
	 */
	public static void main(String[] args){
		
			System.out.println("Introdueix la clau: ");
		try{
			clau = teclat.readLine();
		}catch(IOException e){
			e.printStackTrace();
		}
		
			System.out.println("Tria una de les 3 opcions: ");
			System.out.println("\t1. Vector estatic.");
			System.out.println("\t2. Cua circular estatica.");
			System.out.println("\t3. Memoria dinamica.");
		try{	
			opcio=Integer.parseInt(teclat.readLine());
			
			while(opcio<1 || opcio>3){
				System.out.println("Has d'introduir un numero entre 1 i 3: ");
				opcio=Integer.parseInt(teclat.readLine());
			}
			
		}catch(NumberFormatException e){
			System.out.println("ERROR! Ha d'introduir un numero!");
		}catch(IOException e){
			e.printStackTrace();
		}
			
		
		switch(opcio){
		case 1: cua = new CuaVector<Integer>(clau.length());			break;
		case 2: cua = new CuaCircular<Integer>(clau.length());			break;
		case 3: cua = new CuaDinamica<Integer>();						break;
		}
		
			System.out.println("Introdueix el nom del fitxer: ");
		try{
			nomFitxer = teclat.readLine();
			llegirFitxer();
		}catch(IOException e){
			e.printStackTrace();
		}
			
			System.out.println("Vols xifrar o desxifrar?");
			System.out.println("\t1.Xifrar.");
			System.out.println("\t2.Desxifrar.");
		try{
			mode = Integer.parseInt(teclat.readLine());
		}catch(NumberFormatException e){
			System.out.println("ERROR! Ha d'introduir un numero!");
		}catch(IOException e){
			e.printStackTrace();
		}
		
		switch(mode){
		case 1: xifrar();
				guardaXifrat(textFinal);
				System.out.println("El temps per a xifrar ha sigut de "+(tempsF-tempsI)+" ns.");
				break;
		case 2: desxifrar();
				guardaDesxifrat(textFinal);
				System.out.println("El temps per a desxifrar ha sigut de "+(tempsF-tempsI)+" ns.");
				break;
		}
	
	}
	
	/** Afegeix la clau a la cua triada previament i xifra caracter a caracter el text del fitxer desencuant un valor de la clau per a cada 
	 * caracter. Te en consideracio espais, majuscules i minuscules. Tambe guarda el temps d'inici i final del xifratge. **/
	public static void xifrar(){
		
		tempsI=System.nanoTime();
		
		int valor=-1;
		int caracter;
		char car;
		try{
			for(int i=0; i<clau.length(); i++){
				int a=Character.getNumericValue(clau.charAt(i));
				cua.Encuar(a);
			}
		}catch(CuaPlena e){
			e.getMessage();
		}
		
		for(int i=0; i<textFitxer.length(); i++){
			caracter=Character.valueOf(textFitxer.charAt(i));
			
			if(caracter != 32){
				if(caracter>96 && caracter<123){						//tractament minuscules
					try{
					valor=cua.primer();
					cua.Desencuar();
					cua.Encuar(valor);
					}catch(CuaBuida e){
						e.getMessage();
					}catch(CuaPlena e){
						e.getMessage();
					}
					caracter=caracter+valor;
					if (caracter>122) caracter=96+(caracter-122);
					car=(char) caracter;
				}
				else if(caracter>64 && caracter<91){					//tractament majuscules
					try{
						valor=cua.primer();
						cua.Desencuar();
						cua.Encuar(valor);
						}catch(CuaBuida e){
							e.getMessage();
						}catch(CuaPlena e){
							e.getMessage();
						}
					caracter=caracter+valor;
					if (caracter>90) caracter=64+(caracter-90);
					car=(char) caracter;
				}
				else car= (char) caracter;								//altres caracters
			}
			else car=32;												//tractament espais
			if(i==0) textFinal=Character.toString(car);
			else textFinal=textFinal+car;
		}
		
		tempsF=System.nanoTime();
		
	}
	
	/** Afegeix la clau a la cua triada previament i desxifra caracter a caracter el text del fitxer desencuant un valor de la clau per a cada 
	 * caracter. Te en consideracio espais, majuscules i minuscules. Tambe guarda el temps d'inici i final del desxifratge. **/
	public static void desxifrar(){
		tempsI=System.nanoTime();
		
		int valor=-1;;
		int caracter;
		char car;
		
		try{
		for(int i=0; i<clau.length(); i++){
			int a=Character.getNumericValue(clau.charAt(i));
			cua.Encuar(a);	
		}
		}catch(CuaPlena e){
			e.getMessage();
		}
		
		for(int i=0; i<textFitxer.length(); i++){
			caracter=Character.valueOf(textFitxer.charAt(i));
			
			if(caracter != 32){
				if(caracter>96 && caracter<123){						//tractament minuscules
					try{
					valor=cua.primer();
					cua.Desencuar();
					cua.Encuar(valor);
					}catch(CuaBuida e){
						e.getMessage();
					}catch(CuaPlena e){
						e.getMessage();
					}
					caracter=caracter-valor;
					if (caracter<97) caracter=123-(97-caracter);
					car=(char) caracter;
				}
				else if(caracter>64 && caracter<91){					//tractament majuscules
					try{
					valor=cua.primer();
					cua.Desencuar();
					cua.Encuar(valor);
					}catch(CuaBuida e){
						e.getMessage();
					}catch(CuaPlena e){
						e.getMessage();
					}
					caracter=caracter-valor;
					if (caracter<65) caracter=91-(65-caracter);
					car=(char) caracter;
				}
				else car= (char) caracter;								//altres caracters
			}
			else car=32;												//tractament espais
			if(i==0) textFinal=Character.toString(car);
			else textFinal=textFinal+car;
		}
		
		tempsF=System.nanoTime();
	}
	
	/**
	 * Metode per a guardar un xifrat al fitxer corresponent
	 * @param xifrat es l'string a guardar
	 */
	public static void guardaXifrat(String xifrat){
		try{
			BufferedWriter escriptura= new BufferedWriter(new FileWriter("src/Fitxers/"+nomFitxer+"_vX.txt"));
			escriptura.write(xifrat);
			escriptura.close();
		}catch(FileNotFoundException e){
			System.out.println("ERROR! El fitxer no existeix!");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Metode per a guardar el desxifrat al fitxer corresponent
	 * @param desxifrat es l'string a guardar al fitxer
	 */
	public static void guardaDesxifrat(String desxifrat){
		try{
			BufferedWriter escriptura= new BufferedWriter(new FileWriter("src/Fitxers/"+nomFitxer+"_vD.txt"));
			escriptura.write(desxifrat);
			escriptura.close();
		}catch(FileNotFoundException e){
			System.out.println("ERROR! El fitxer no existeix!");
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Metode per a llegir d'un fitxer l'string
	 */
	public static void llegirFitxer(){
		try{
			BufferedReader lectura= new BufferedReader(new FileReader("src/Fitxers/"+nomFitxer+".txt"));
			StringTokenizer token;
			String linia;
			linia=lectura.readLine();
			token=new StringTokenizer(linia, " ");
			while(linia!=null){
				textFitxer=token.nextToken();
				while(token.hasMoreTokens()){
					textFitxer=textFitxer+" "+token.nextToken();
				}
				linia=lectura.readLine();
			}
			lectura.close();
		}catch(FileNotFoundException e){
			System.out.println("ERROR! No s'ha trobat el fitxer!");
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
