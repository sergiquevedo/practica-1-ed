import Exceptions.CuaBuida;
import Exceptions.CuaPlena;

/**
 * Interface per a definir la classe de la cua gen�rica  amb els seus m�todes
 * @author Sergi Quevedo i Javier Ortega
 */
public interface TADCuaGenerica<E> {
	
	/**
	 * Afegeix un element a la cua
	 * @param e �s l'element a encuar
	 * @throws CuaPlena si la cua es plena
	 */
	public void Encuar (E e) throws CuaPlena;
	
	/**
	 * Desencua un element de la cua
	 * @return l'element desencuat
	 * @throws CuaBuida si la cua es buida
	 */
	public E Desencuar() throws CuaBuida;
	
	/**
	 * M�tode per saber si la cua �s plena
	 * @return cert si �s plena i fals si no �s plena
	 * @throws CuaBuida si la cua es buida
	 */
	public E primer() throws CuaBuida;
	
	/**
	 * M�tode per saber si la cua �s buida
	 * @return cert si la cua �s buida i fals si no �s buida
	 */
	public boolean esBuida();
	
	/**
	 * Retorna el n�mero d'elements guardats a la cua
	 * @return el n�mero d'elements guardats a la cua
	 */
	public int getNumElem();
}
